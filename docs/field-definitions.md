---
title: Field Definitions
---

# Field Definitions

## Description

 - A single sentence comprising key pieces of information which serves as a ‘top line’ summary from which a clinician will gain the most information at a glance.
 - Might be expressed in a lab report comment or read quickly in reviewing results before clinic.
 - May include some or all of previous definitions including but not limited to target antigen, expression pattern, antigen location, molecular characteristics.

 - <small>E.g. “NF155 is an autoantibody usually of the IgG4 isotype associated with a rare autoimmune paranodopathy presenting with ataxic neuropathy formerly considered part of the spectrum of CIDP.”</small>
 - <small>E.g. “Anti-SOX1 is an autoantibody directed against an intracellular antigen highly expressed in the cerebellar cortex, 30% of positive cases have Lambert-Eaton myasthenic syndrome, and 85% of positive cases have concomitant small cell lung cancer.”</small>
 - <small>E.g. “MOG is the target of an IgG antibody which may be found in anti-AQ4 negative neuromyelitis optica spectrum disorder and 20-40% of cases of acute disseminated encephalomyelitis.”</small>

 
## Autoantibody characteristics

 - [Mechanism of action](#) detailing how the antibody is thought to be pathological (e.g. anti-NMDAR cross linking and internalisation of the receptor) or, as in the case of some onconeuronal antibodies, that we may not know if it is a pathological autoantibody and that the pathological mechanism may be something else (e.g. cytotoxic T cell mediated).
 - [Clinical features](#) which includes symptoms and signs.
 - [Associated neoplasia](#) to describe the most common malignancies associated with the antibody and to identify whether the antibody is high <small>(>70%)</small>, intermediate <small>(30-70%)</small>, or lower risk <small>(<30%)</small> for an associated malignancy.
 
## Assays

 - [Type of assay](#) - indirect immunofluorescence, cell based assay, flow cytometry, radioimmunoprecipitation/fluorescence immunoprecipitation, ELISA.
 - [Sensitivity and specificity](#)
 - [Grade](#)

## Prognosis of clinical syndrome and response to treatment

---

<small>
## Changelog

 - Added [description](#)
 - Removed [function](#), [antigen location](#) and [expression pattern](#) as these are not clinically relevant and do not assist with pre-analysis test ordering
 - Removed [target antigen](#) as this is usually expressed in the name and does not need to be clarified
 - Removed [antigen location](#)
 - Removed [molecular characteristics](#) as this does not need to be it’s own category and should be expressed in description
 - Combined signs and symptoms into [clinical features](#) for brevity
 - Removed [where to get it](#) from Assays as this will not be possible internationally and the CDSS is not limited to Australia
 - Removed [notes](#) which serves no purpose
 - Changed [recommendation](#) to [grade](#) for clarity

</small>

---
