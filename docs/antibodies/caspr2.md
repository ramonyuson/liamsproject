# Contactin-associated protein-like 2 (CASPR2) (VGKC complex)

Contactin-associated protein-2 (CASPR2) antibody

## Molecular characteristics
Often IgG4

## Site of action
Cell surface, Neighbouring protein stabilising the ion channel

## Mechanism of action
CASPR2 and contactin2 antibodies inhibit the interaction between cell surface molecules and recude the clustering and surface expression of VGKC

## Associated conditions
Morvan syndrome

## Associated neoplasia
10-40% associated (44% for LGI1 and CASPR2 dual seropositivty), thymoma

## Common clinical features
Neuromyotonia, seizure in 75%, neuropathic pain, dysautonomic, limbic encephalitis, hyponatraemia in 25%, diarrhoea

## Assay 1

## Assay 2

## Prognosis
!!! info
    Good response to immunotherapy, relapse rate 35%

## Notes
!!! note
    With standard diagnostic criteria fulfilling clinical and investigation criteria with reasonable exclusion of all other causes the sensitivity is 81.2%, specificity 76.9%
