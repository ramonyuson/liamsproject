# Leucine-rich, glioma inactvated protein 1 (LGI1) (VGKC complex)

## Molecular characteristics
Often IgG4

## Site of action
Cell surface, Neighbouring protein stabilising the ion channel

## Mechanism of action
LGI1 antibody disrupts interaction between protein components as LGI1 to ADAM22/23, downregulated VGKC, and reduces AMPAR clustering and synaptic transmission

## Associated conditions

## Associated neoplasia
<10% SCLC, thymoma

## Common clinical features
Seizure in 80-100%, limbic encephalitis, hyponatraemia in 50%, dizziness in 14%, neuropathic pain or cramps, dysautonomia, diarrhoea

## Assay 1

## Assay 2

## Prognosis
Good response to immunotherapy, relapse rate 15-35%

## Notes

