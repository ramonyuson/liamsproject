# NMDAR

## Molecular characteristics
IgG1

## Site of action
N-methyl-D-aspartate receptor

## Mechanism of action
Cross linking and receptor internalisation

## Associated conditions

## Associated neoplasia

## Common clinical features
Rapid onset (<3m) of abnormal psychiatric/cognitive symptoms, speech dysfunction, seizures in 70%, movement disorders, dyskinesia, rigidity/abnormal postures, decreased level of consciousness, autonomic dysfunction, central hypoventilation, abnormal EEG, CSF with pleiocytosis, CSF with oligoclonal bands, reasonable exclusion of other disorders, diarrhoea

## Assay 1

## Assay 2

## Prognosis

## Notes
With standard diagnostic criteria fulfilling clinical and investigation criteria with reasonable exclusion of all other causes the sensitivity is 81.2%, specificity 76.9%
