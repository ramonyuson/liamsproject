# DNA Antibody (Anti-Double Stranded DNA)

## Assay Grade
[=90% "A-"]{: .candystripe}

## Description
Anti-(double stranded)-DNA antibodies are highly specific markers of SLE and autoimmune (lupoid) hepatitis. Raised levels are found in 50-70% of patients with active, particularly untreated, SLE. Some patients with SLE (40-60%) however, never develop anti-DNA antibodies. All ANA positive samples will automatically be tested for anti-DNA antibodies. The test will only be performed on ANA positive samples. Most patients with anti-DNA antibodies have high levels of homogeneous or rim pattern anti-nuclear antibodies. Though this is a highly specific marker of SLE, many patients with SLE who have appropriate ENA’s do not have anti-DNA. In those SLE patients with anti-DNA antibodies, the levels tend to correlate with disease activity though the antibodies can disappear after prolonged treatment.The assay used in this laboratory uses recombinant double stranded DNA. Anti-DNA antibodies are, in fact, a heterogeneous group of antibodies with differing specificities. Earlier differentation between antibodies recognising double and single stranded DNA was not as simple as suggested and is not useful in separating SLE from drug induced disease.

Lupus patients with anti-DNA antibodies frequently have low levels of complement and active nephritis. Very high levels of anti-DNA antibodies (often higher than in SLE) are found in some patients chronic active (lupoid) hepatitis.

## Indication
 - Diagnosis and monitoring of SLE, autoimmune liver disease.

## Interpretation
Raised levels are found in 50-70% of patients with SLE and tend to correlate with disease activity. Raised levels may also be seen in a small minority of patients with chronic active hepatitis. The presence of anti-DNA is one of the American Rheumatism Association criteria for SLE and the antibody is rarely found in high levels in patients with other rheumatic diseases. However, anti-dsDNA antibodies are frequently found in patients with overlap symptoms between SLE and other autoimmune diseases. In different SLE patients, relative levels of anti-DNA antibody do not correlate well with relative disease activity although rapid rises in antibody levels in any individual patient do suggest increases in disease activity particularly glomerulonephritis.

## Sample
Serum Separator Tube (SST)

## Assay details
Fluorescence enzyme linked immunoassay (Phadia Immunocap 250).: Double stranded plasmid DNA: IgG antibodies.

## Restrictions
!!! warning

    Serum is preferred but heparinised plasma may be used as a last resort.


## Reference range
`<10IU/mL`

## Assay range notes
| Value      | Description                          |
| ----------: | ------------------------------------ |
| `10-15IU/mL`      | :material-close:     Borderline  |
| `>15IU/mL`       | :material-check: Positive |
| `>400IU/ml`    | :material-check-all:     Maximum |

## Turnaround time
5 - 7 days

## Analysing laboratory
```
Immunology Concord Repatriation General Hospital
Immunology Royal Prince Alfred Hospital
```

[^1]: DNA Antibody (Anti-Double Stranded DNA). (2013, August 16). South Tees Hospitals NHS Foundation Trust. Retrieved December 16, 2021, from https://www.southtees.nhs.uk/services/pathology/tests/dna-antibody-anti-double-stranded-dna/ 
