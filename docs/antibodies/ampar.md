# AMPAR

α-amino-3-hydroxy-5-methyl-4-isoxazolepropionic acid receptor (also known as AMPA receptor, AMPAR, or quisqualate receptor)

## Molecular characteristics
IgG1

*[IgG1]: Immunoglobulin G1

## Site of action
Cell surface

## Mechanism of action
Cross linking and receptor internalisation

## Associated conditions


## Associated neoplasia
50-70%, most commonly lung and thymoma but also breast

## Common clinical features
Most commonly confusion, then limbic encephalitis, psychiatric symptoms, seizure in 33%

## Assay 1

## Assay 2

## Prognosis
!!! info
    Favourable reponse to immunotherapy, commonly relapses

## Notes
