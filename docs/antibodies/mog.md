# MOG

## Molecular characteristics
IgG

## Site of action
Outer surface of myelitin sheath in the CNS

## Mechanism of action

## Associated conditions
Acute disseminated encephalomyelitis (ADEM) (anti-MOG present in 20-40% of cases), seizures, encephalitis, aquaporin-4 IgG negative NMOSP, unclear if associated with MS

## Associated neoplasia

## Common clinical features
Optic neuritis, myelitis, brainstem encephalitis,

## Assay 1
Kim et al Cell based assay: MOG transfected cell line and cell based assay, high concordance with In-house and Oxford cell based assay, IgG H+L and IgG1-Fc antibodies comparably binded and there was no IgM binding, no MS/AQP4-IgG positive NMOSD or healthy individuals were MOG-IgG seropositive.

## Assay 2

## Prognosis

## Notes
